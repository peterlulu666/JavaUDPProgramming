How to run the program:
1. In client
Javac *.java
java client.java
2. In server
Javac *.java
java server.java

How to use the program:
1. Open the new terminal and change directory to server, run the program in terminal, and change directory to serverDoc
2. Copy all the code in client1 and paste it to client2, client3 and so on
3. Open the new terminal and change directory to client1, run the program in terminal, and change directory to clientDoc
4. Open the new terminal and repeat the same process for other client
5. Move any file to client1 clientDoc, then you can view the file in serverDoc, then you can view the file in other clientDoc
6. Change the content of file in client1, then you can view the change in server and other client
7. Remove the file in client1, then it tells the server to remove it, then the server tells other client to remove it





